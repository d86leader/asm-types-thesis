%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Стилевой файл для набора дипломной работы.
%
% Copyright (C) 2008, 2009 Дмитрий Стефанцов
% Copyright (C) 2008, 2009 Dmitry Stephantsov
% Copyright (С) 2019, 2019 d86leader
% Copyright (С) 2020, 2020 Михаил Муругов
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
% Об ошибках и предложениях сообщайте Стефанцову Д.А.,
% email: d.a.stephantsov@gmail.com
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{diploma}[2008/14/12 Diploma]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
\ProcessOptions

\LoadClass{report}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Подключение стандартных пакетов
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage[warn]{mathtext}
\RequirePackage[T2A]{fontenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage[english,russian]{babel}
\RequirePackage{indentfirst}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{graphicx}
\RequirePackage{geometry}
\RequirePackage{ifthen}
\RequirePackage[titles]{tocloft}
\RequirePackage{listings}
\RequirePackage[normal,nooneline]{caption}
\RequirePackage{algorithm}
\RequirePackage{algorithmic}
\RequirePackage{tabularx}
\RequirePackage{longtable}
\RequirePackage[unicode=true]{hyperref}
\RequirePackage{color}
\RequirePackage{mathrsfs}
\RequirePackage{theorem}
\RequirePackage{lastpage}
\RequirePackage{soulutf8}
\RequirePackage{csquotes}
\RequirePackage[backend=biber,sorting=none,citestyle=numeric-comp,bibstyle=gost-numeric]{biblatex}
\RequirePackage{enumitem}
\RequirePackage{setspace}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Настраиваем расположение текста на странице
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% поля (ориентируемся на размер бумаги A4)
\geometry{%
  paper=a4paper,
  left=30mm,
  right=10mm,
  top=20mm,
  bottom=20mm,
}

% полуторный межстрочный интервал
\linespread{1.3}

% величина отступа "красной строки"
\parindent=15mm

% подавляем эффект "висячих строк"
\clubpenalty=10000
\widowpenalty=10000

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Переопределяем команду оформления заголовка статьи --
%   теперь она выводит титульный лист работы
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% комманды-обёртки для форматирования вывода полей титульного листа
\newcommand{\typeOrganization}[1]{%
  \ifthenelse{\isundefined{#1}}{}{{#1}}}
\newcommand{\typeUniversity}[1]{%
  \ifthenelse{\isundefined{#1}}{}{{\MakeUppercase{#1}}}}
\newcommand{\typeFaculty}[1]{%
  \ifthenelse{\isundefined{#1}}{}{{#1}}}
\newcommand{\typeDepartment}[1]{%
  \ifthenelse{\isundefined{#1}}{}{{#1}}}
\newcommand{\typeUdk}[1]{%
  \ifthenelse{\isundefined{#1}}{}{{\MakeUppercase{УДК #1}}}}
\newcommand{\typeHeadofdep}[4]{%
  \ifthenelse{\isundefined{#1}}{}{%
    {%
      \large
      \MakeUppercase{#3} \\
      #2 \\
      \rule{3cm}{0.4pt} #1 \\
      << \rule{1cm}{0.4pt} >> \rule{4cm}{0.4pt} #4 г.
    }
  }
}
\newcommand{\typeHeadofdepAllow}[2]{%
  \typeHeadofdep{#1}{#2}{Допустить к защите в ГЭК}{2021}%
}
\newcommand{\typeHeadofdepAccept}[2]{%
  \typeHeadofdep{#1}{#2}{Утверждаю}{2020}%
}
\newcommand{\typeSupertitle}[1]{%
  \ifthenelse{\isundefined{#1}}{}{{\MakeUppercase{\textbf{#1}}}}}
\newcommand{\typeTitle}[1]{{\MakeUppercase{#1}}}
\newcommand{\typeProgram}[1]{
    \begin{spacing}{1}
        #1
    \end{spacing}
}
\newcommand{\typeSubtitle}[1]{%
  \ifthenelse{\isundefined{#1}}{}{{#1}}}
\newcommand{\typeAuthor}[2]{%
  {%
    Автор работы \\
    #2 \\
    \rule{3cm}{0.4pt} #1
  }
}
\newcommand{\typeSupervisor}[2]{%
  {%
    Руководитель ВКР \\
    #2 \\
    \rule{3cm}{0.4pt} #1 \\
    << \rule{1cm}{0.4pt} >> \rule{4cm}{0.4pt} 2021 г.
  }
}
\newcommand{\typeCity}[1]{{#1}}

% (пере)определения команд для указания полей титульного листа
\newcommand{\organization}[1]{\gdef\@organization{#1}}
\newcommand{\university}[1]{\gdef\@university{#1}}
\newcommand{\faculty}[1]{\gdef\@faculty{#1}}
\newcommand{\department}[1]{\gdef\@department{#1}}
\newcommand{\udk}[1]{\gdef\@udk{#1}}
\newcommand{\headofdep}[2]{%
  \gdef\@headofdep{#1}%
  \gdef\@headofdepPosition{#2}%
}
\newcommand{\program}[1]{\gdef\@program{#1}}
\newcommand{\supertitle}[1]{\gdef\@supertitle{#1}}
\renewcommand{\title}[1]{\gdef\@title{#1}}
\newcommand{\subtitle}[1]{\gdef\@subtitle{#1}}
\renewcommand{\author}[2]{%
  \gdef\@author{#1}%
  \gdef\@authorPosition{#2}%
}
\newcommand{\supervisor}[2]{%
  \gdef\@supervisor{#1}%
  \gdef\@supervisorPosition{#2}%
}
\newcommand{\city}[1]{\gdef\@city{#1}}

% определяем пустые задачу и реферат, чтобы документ транслировался
% при их отсутствии
\gdef\@task{~}
\gdef\@referat{~}

% переопределение команды вывода заголовка (на вывод титульного листа)
\def\maketitle{%
  % номер страницы не ставится на титульном листе
  \thispagestyle{empty}

  \begin{center}
    \typeOrganization{\@organization} \\
    \typeUniversity{\@university} \\
    \typeFaculty{\@faculty} \\
    \typeDepartment{\@department}
  \end{center}

  \vspace{1cm}
  
  \begin{flushright}
    \begin{minipage}[h]{8cm}
      \typeHeadofdepAllow{\@headofdep}{\@headofdepPosition}
    \end{minipage}
  \end{flushright}
  
  \vspace{\fill}

  \begin{center}
    \typeSupertitle{\@supertitle} \\
    \vspace{0.5cm}
    \typeTitle{\@title} \\
    \vspace{0.5cm}
	\typeProgram{\@program} \par
	\vspace{0.5cm}
    \typeSubtitle{\@subtitle}
  \end{center}

  \vspace{\fill}

  \begin{flushright}
    \begin{minipage}[h]{8cm}
      \typeSupervisor{\@supervisor}{\@supervisorPosition} \vspace{1cm} \\
      \typeAuthor{\@author}{\@authorPosition} \\
    \end{minipage}
  \end{flushright}

  \vspace{1cm}

  \begin{center}
    \typeCity{\@city}
  \end{center}
 
  % на следующей странице -- задание на выполнение работы
  \pagebreak
  \begin{center}
    \typeOrganization{\@organization} \\
    \typeUniversity{\@university} \\
    \typeFaculty{\@faculty} \\
    \typeDepartment{\@department}
  \end{center}
  \vspace{5mm}
  \begin{flushright}
    \begin{minipage}[h]{8cm}
      \typeHeadofdepAccept{\@headofdep}{\@headofdepPosition}
    \end{minipage}
  \end{flushright}
  \vspace{5mm}
  \begin{center}
    \textbf{\MakeUppercase{Задание}}
  \end{center}
  \vspace{-5mm}
  \@task
  \vspace{5mm}
  \noindent Руководитель ВКР\\
  \noindent \@supervisorPosition \hfill \rule{5cm}{0.4pt} / \@supervisor /\\
  \noindent Задание принял к исполнению\\
  \noindent \@authorPosition \hfill \rule{5cm}{0.4pt} / \@author /
  % на следующей странице -- реферат
  \pagebreak
  {%
    \begin{center}
        \textbf{\MakeUppercase{Реферат}}\par
    \end{center}
    \vspace{4mm}
  }
  \@referat
  % на следующей странице -- аннотации на русском и английском
  %   (если есть)
  \pagebreak
  \typeRuAbstract{\@ruabstract}
  \typeEnAbstract{\@enabstract}
}

\newcommand{\task}[1]{%
  \gdef\@task{#1}%
}

\newcommand{\referat}[1]{%
  \gdef\@referat{#1}%
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Настройка пред- и постдействий
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\gdef\totalFigures{XX}

\newcommand{\@BeginDocument}{%
}
\newcommand{\@EndDocument}{%
  \gdef\totalFigures{\arabic{figure}}%
}

\let\@olddocument\document
\let\@oldenddocument\enddocument
\renewenvironment{document}{%
  \@olddocument
  \@BeginDocument
}{%
  \@EndDocument
  \@oldenddocument
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Настройка отображения аннотации
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% заголовки аннотации на русском и английском языках
\newcommand{\ruabstractname}[0]{Аннотация}
\newcommand{\enabstractname}[0]{Abstract}

% форматирование отображения заголовка аннотации
\newcommand{\typeAbstractName}[1]{%
  {%
    \indent \raggedright
    \Large \scshape \bfseries #1\par
    \vspace{4mm}
  }
}

% команда отображения аннотации
\newcommand{\typeAbstract}[2]{%
  \ifthenelse{\isundefined{#2}}{}{%
    \typeAbstractName{#1}
    \noindent #2\par
    \vspace{2cm}
  }
}

% отображение аннотации на русском и английском языках
\newcommand{\typeRuAbstract}[1]{\typeAbstract{\ruabstractname}{#1}}
\newcommand{\typeEnAbstract}[1]{%
    \selectlanguage{english}%
    \typeAbstract{\enabstractname}{#1}%
    \selectlanguage{russian}
}

% команды для определения аннотаций на русском и английском языках
\newcommand{\ruabstract}[1]{\gdef\@ruabstract{#1}}
\newcommand{\enabstract}[1]{\gdef\@enabstract{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Настройка стиля глав и оглавления
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% меняем отображение названия главы
\renewcommand{\@makechapterhead}[1]{%
  {%
    \raggedright
    % \Large \scshape 
    \bfseries \scshape
    \renewcommand{\@listi}{%
      \setlength{\leftmargin}{18mm}
      \addtolength{\leftmargin}{\labelsep}
      \labelsep=1.5ex
    }
    \begin{list}{}{}
      \item[\thechapter] #1
    \end{list}
  }
}
\renewcommand{\contentsname}{}
% то же для глав без номера
\renewcommand{\@makeschapterhead}[1]{%
%   {%
% %     \ifthenelse{\equal{#1}{\bibname}\or\equal{#1}{\contentsname}}%
% %                {}%
% %                {\indent}%
% 	\indent%
%     \raggedright%
%     \Large \scshape \bfseries #1\par
%     \vspace{4mm}
%   }
  {%

    \raggedright
    \bfseries 
    \begin{list}{}{}\item[]\par
        \ifthenelse{\equal{#1}{\contentsname}}{%
    \centering \MakeUppercase{#1}
  }{%
    \centering    #1
  }
    \end{list}
  }
    

    
%   % ненумерованные главы добавляем в оглавление
%   %   (кроме самого оглавления)

}

% меняем оформление названия разделов (секций)
\renewcommand{\section}{%
  \@startsection{section}% тип раздела
                {1}% уровень раздела
                {\parindent}% отступ "красной строки"
                {-\baselineskip}% верт. отступ перед названием
                {0.25\baselineskip}% верт. отступ после названия
                {\normalfont\scshape\raggedright\parindent=15mm}% команды форматирования
}

% меняем оформление названия подразделов (подсекций)
\renewcommand{\subsection}{%
  \@startsection{subsection}% тип раздела
                {2}% уровень раздела
                {\parindent}% отступ "красной строки"
                {-\baselineskip}% верт. отступ перед названием
                {0.25\baselineskip}% верт. отступ после названия
                {\normalfont\scshape\raggedright\parindent=15mm}% команды форматирования
}

% меняем оформление названия подподразделов (подподсекций)
\renewcommand{\subsubsection}{%
  \@startsection{subsubsection}% тип раздела
                {3}% уровень раздела
                {\parindent}% отступ "красной строки"
                {-\baselineskip}% верт. отступ перед названием
                {0.25\baselineskip}% верт. отступ после названия
                {\normalfont\scshape\raggedright\parindent=15mm}% команды форматирования
}

% меняем оформление названия параграфов
\renewcommand{\paragraph}{%
  \@startsection{paragraph}% тип раздела
                {4}% уровень раздела
                {\parindent}% отступ "красной строки"
                {-\baselineskip}% верт. отступ перед названием
                {0.25\baselineskip}% верт. отступ после названия
                {\normalfont\bfseries\raggedright\parindent=15mm}% команды форматирования
}

% счётчик для приложений
\newcounter{appendix}
\setcounter{appendix}{0}
\renewcommand{\theappendix}{\Asbuk{appendix}}

%команда для оформления приложения
\renewcommand{\appendix}[1]{%
	\addtocounter{appendix}{1}
	\chapter*{ПРИЛОЖЕНИЕ~\theappendix\\#1}
	\addcontentsline{toc}{chapter}{ПРИЛОЖЕНИЕ~\theappendix~#1}
}

% убираем линии из точек для разделов (секций) в оглавлении
\renewcommand{\cftsecdotsep}{\cftnodots}
\renewcommand{\cftsubsecdotsep}{\cftnodots}

% меняем оформление оглавления
\let\oldl@chapter\l@chapter
\renewcommand{\l@chapter}[2]{%
  \oldl@chapter{{\normalfont #1}}{{\normalfont #2}}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Настройка отображения списков
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Нумерованный перечень со скобками
\newenvironment{enumerate*}
{%
  \begingroup
  \renewcommand{\@listi}{%
    \topsep=\smallskipamount % вокруг списка
    \parsep=0pt% между абзацами внутри пункта
    \parskip=0pt% между абзацами
    \itemsep=0pt% между пунктами
    \itemindent=0ex% абзацный выступ
    \labelsep=1.5ex% расстояние до метки
    \setlength{\leftmargin}{\parindent}% отступ слева
    %\addtolength{\leftmargin}{\labelwidth}
    \rightmargin=0pt} % отступ справа
  \begin{enumerate}%
  \renewcommand\labelenumi{\theenumi)}%
  \renewcommand\labelenumii{\asbuk{enumii})}%
}{%
  \end{enumerate}%
  \endgroup
}

% Ненумерованный перечень
\newenvironment{itemize*}
{%
   \begingroup
   \renewcommand{\@listi}{%
    \topsep=\smallskipamount % вокруг списка
    \parsep=0pt% между абзацами внутри пункта
    \parskip=0pt% между абзацами
    \itemsep=0pt% между пунктами
    \itemindent=0ex% абзацный выступ
    \labelsep=1.5ex% расстояние до метки
    \setlength{\leftmargin}{\parindent}% отступ слева
    %\addtolength{\leftmargin}{\labelwidth}
    \rightmargin=0pt} % отступ справа
%  \renewcommand{\@listii}{\@listi\topsep=0pt}%
%  \renewcommand{\@listiii}{\@listii}%
%  \renewcommand{\@listiv}{\@listii}%
    \begin{itemize}
      \renewcommand{\labelitemi}{---}%
      \renewcommand{\labelitemii}{---}%
      \renewcommand{\labelitemiii}{---}%
      \renewcommand{\labelitemiv}{---}%
}{%
    \end{itemize}
    \endgroup
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Настройка отображения математики
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\renewcommand{\theequation}{\arabic{equation}}
\renewcommand{\emptyset}{\varnothing}

% отображение некоторых греческих букв способом, принятым в
%   русской типографии
\renewcommand{\phi}{\varphi}
\renewcommand{\epsilon}{\varepsilon}
%\renewcommand{\kappa}{\varkappa}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Настройка алгоритмов, отображаемых в псевдокоде
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% русифицируем алгоритм
\renewcommand{\algorithmicrequire}{\textbf{Вход:}}
\renewcommand{\algorithmicensure}{\textbf{Выход:}}
\renewcommand{\algorithmicwhile}{\textbf{Пока}}
\renewcommand{\algorithmicendwhile}{\textbf{Конец цикла.}}
\renewcommand{\algorithmicdo}{\textbf{}}
\renewcommand{\algorithmicif}{\textbf{Если}}
\renewcommand{\algorithmicthen}{\textbf{то}}
\renewcommand{\algorithmicendif}{\textbf{Конец условия.}}
\renewcommand{\algorithmicreturn}{\textbf{Вернуть}}
\floatname{algorithm}{Алгоритм}
\newcommand{\theHalgorithm}{\arabic{algorithm}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Настройка пакета listings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\lstset{%
    numbers=left,
    %frame=lines,
    breaklines=true,
    breakatwhitespace=true,
    captionpos=b,
    columns=fixed,
    flexiblecolumns=false,
    keepspaces=true,
    basicstyle={\small\ttfamily},
    keywordstyle={\small\ttfamily\bfseries},
    xleftmargin=1cm,
    xrightmargin=1cm,
    float,
    escapeinside={(*@}{@*)},
    aboveskip=0pt,
    belowskip=0pt,
    showstringspaces=false,
    float=tbhp,
    tabsize=4,
}
\renewcommand{\lstlistingname}{Листинг}
%\renewcommand{\thelstlisting}{\arabic{lstlisting}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Настройка сквозной нумерации счётчиков
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% новый счётчик и команда отображения номера для листингов
\newcounter{diplomaLstlisting}
\AtBeginDocument{\renewcommand*\thelstlisting{\arabic{lstlisting}}}

% новый счётчик и команда отображения номера для уравнений
\newcounter{diplomaEquation}
\renewcommand*\theequation{\arabic{equation}}

% новый счётчик и команда отображения номера для рисунков
\newcounter{diplomaFigure}
\renewcommand*\thefigure{\arabic{figure}}

% новый счётчик и команда отображения номера для таблиц
\newcounter{diplomaTable}
\renewcommand*\thetable{\arabic{table}}

\let\oldchapter=\chapter
\renewcommand*{\chapter}{\secdef{\Chap}{\ChapS}}
\newcommand\ChapS[1]{\oldchapter*{#1}}
\newcommand\Chap[2][]{%
	% сохраняем счётчик для листингов
	\setcounter{diplomaLstlisting}{\value{lstlisting}}%
	% сохраняем счётчик для уравнений
	\setcounter{diplomaEquation}{\value{equation}}%
	% сохраняем счётчик для рисунков
	\setcounter{diplomaFigure}{\value{figure}}%
	% сохраняем счётчик для таблиц
	\setcounter{diplomaTable}{\value{table}}
	% вызываем оригинальную команду
	\oldchapter[#1]{#2}%
	% восстанавливаем счётчик для листингов
	\setcounter{lstlisting}{\value{diplomaLstlisting}}%
	% восстанавливаем счётчик для уравнений
	\setcounter{equation}{\value{diplomaEquation}}%
	% восстанавливаем счётчик для рисунков
	\setcounter{figure}{\value{diplomaFigure}}
	% восстанавливаем счётчик для таблиц
	\setcounter{table}{\value{diplomaTable}}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Настройка подписей
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% подписи для рисунков
\DeclareCaptionLabelSeparator{emdash}{\,---\,}
\DeclareCaptionLabelFormat{diplomaFigureCaptionLabelFormat}{Рисунок~#2}


% \captionsetup[table]{
%     position=top,
%     aboveskip=5pt,
%     labelsep=space,
%     name={\hfill\so{Таблица }},
% }

\captionsetup[table]{position=top,aboveskip=5pt,labelsep=endash}


% Заголовки для рисунков
\captionsetup[figure]{%
    position=below,
    labelfont={small},
    textfont={small},
    justification=centering,
    labelsep=emdash,
    labelformat=diplomaFigureCaptionLabelFormat,
}
%\renewcommand{\thefigure}{\arabic{figure}}

% Для листингов - точка после слова "Листинг"
\captionsetup[lstlisting]{%
    labelsep=period,
    labelfont={small},
    textfont={small},
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Настройки библиографии
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% меняем стиль нумерации -- номер с точкой вместо номера в кв. скобках
\renewcommand{\@biblabel}[1]{#1.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Дополнительные настройки
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Theorems
\theoremstyle{plain}
\gdef\th@plain{\normalfont
    \def\@begintheorem##1##2{%
        %\item[\hskip\labelsep\theorem@headerfont \hspace*{\parindent} ##1\ ##2. ]}%
        \item[\theorem@headerfont \hspace*{\parindent} ##1\ ##2. ]}%
    \def\@opargbegintheorem##1##2##3{%
        %\item[\hskip\labelsep\theorem@headerfont \hspace*{\parindent} ##1\ ##2 {\normalfont ##3.} ]}%
        \item[\theorem@headerfont \hspace*{\parindent} ##1\ ##2 {\normalfont ##3.} ]}%
}
\theorembodyfont{\rmfamily}
\newtheorem{definition}{Определение}
 
% окружения типа теоремы
\theorempreskipamount=\smallskipamount
\theorembodyfont{\rmfamily}
\newtheorem{Theorem}{Теорема}
\newtheorem{Lemma}{Лемма}
\newtheorem{State}{Утверждение}
\newtheorem{Corollary}{Следствие}
\newtheorem{Def}{Опр.}
\newtheorem{Definition}{Определение}
\newtheorem{Axiom}{Аксиома}
\newtheorem{Hypothesis}{Гипотеза}
\newtheorem{Problem}{Задача}
\newtheorem{Example}{Пример}
\newtheorem{Remark}{Замечание}
\newtheorem{State-rm}{Утверждение}
% окружение для доказательств
\newenvironment{Proof}[0]{%
    {\itshape\bfseries \hspace*{\parindent} Доказательство.}
}
{%
    \qed
    \vskip 7pt
}
% символ Халмоша
\def\qed{\rule{6pt}{6pt}}

% команды для определения математических операторов
\newcommand\myop[1]{\mathop{\operator@font #1}\nolimits}
\newcommand\mylim[1]{\mathop{\operator@font #1}\limits}

% *** теория доменов ***
\newcommand{\inject}{\myop{in}}
\newcommand{\project}{\myop{\mid}}
\newcommand{\inspect}{\myop{E}}


% Math hyphenation
\newcommand*{\hm}[1]{#1\nobreak\discretionary{}%
 {\hbox{$\mathsurround=0pt #1$}}{}}

% Another math definitons
\renewcommand{\geq}{\geqslant}
\renewcommand{\leq}{\leqslant}
\renewcommand{\ge}{\geqslant}
\renewcommand{\le}{\leqslant}
\renewcommand{\emptyset}{\varnothing}
%\renewcommand{\kappa}{\varkappa}
\renewcommand{\phi}{\varphi}
\renewcommand{\epsilon}{\varepsilon}

% прописные греческие буквы
\newcommand{\Epsilon}{\text{E}}
\newcommand{\Iota}{\text{I}}
\newcommand{\Kappa}{\text{K}}
\newcommand{\Nu}{\text{N}}
\newcommand{\Mu}{\text{M}}
\renewcommand{\O}{\text{O}}
\newcommand{\Chi}{\text{X}}
\newcommand{\Beta}{\text{B}}
\newcommand{\Rho}{\text{P}}
\newcommand{\Alpha}{\text{A}}
\newcommand{\Tau}{\text{T}}
\newcommand{\Eta}{\text{H}}
\newcommand{\Zeta}{\text{Z}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Прочее
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\fixme}[1]{%
	{\color{red} #1}%
}

\AddEnumerateCounter{\asbuk}{\russian@alph}{щ}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Конец стилевого файла
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\endinput
