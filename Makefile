all: main.pdf
zip: asm-types.zip

.PHONY: force
force:
	biber main
	pdflatex main.tex

%.pdf: %.tex main.bib commands.tex diploma.cls
	pdflatex $<

%.bbl: %.bib
	biber $(basename $<)

asm-types.zip: commands.tex diploma.cls main.bib main.tex
	zip $@ $^

clean:
	rm main.aux main.bbl main.bcf main.bib.bbl main.bib.blg main.blg main.log main.out main.pdf main.run.xml main.toc missfont.log

clean_bib:
	rm main.bbl main.bcf main.bib.bbl main.bib.blg main.blg
