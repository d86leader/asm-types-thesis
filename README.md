# Asm Types Thesis

Required latex packages:
- texlive-latex-bin
- texlive-biber-bin
- texlive-cyrillic
- texlive-babel-russian
- texlive-tocloft
- texlive-lastpage
- texlive-lh
- texlive-biblatex-gost

Also the makefile might not work on the first try. Build with:
```
pdflatex main.tex
make force
```
After that rebuild with `make`, and if the bibliography or contents don't update, `make force` after that
